package datos;

import java.util.Scanner;

public class Datos {

	public static void main(String[] args) {
		
        Scanner input=new Scanner(System.in);
		
		System.out.println("Introduce tu nombre");
		String nombre=input.nextLine();
		System.out.println("Introduce tu apellido");
		String apellido=input.nextLine();
		
		System.out.println("Tu nombre completo es: "+nombre+" "+apellido);
		
		input.close();

	}

}
